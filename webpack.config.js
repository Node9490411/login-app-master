const path = require('path');

module.exports = {
  entry: './app.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
    globalObject: 'this', // set global object to 'this' to use built-in Node.js modules
  },
  node: {
    assert: true, // set `true` to include the assert module
    util: true, // set `true` to include the util module
    url: true, // set `true` to include the url module
    crypto: true, // set `true` to include the crypto module
  },
  resolve: {
  fallback: {
    http: require.resolve('stream-http'),
    util: require.resolve('util/'),
    crypto: require.resolve('crypto-browserify'),
    buffer: require.resolve('buffer/'),
  }
}
};
